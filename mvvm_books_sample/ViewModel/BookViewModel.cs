﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using mvvm_books_sample.Model;
using mvvm_books_sample.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace mvvm_books_sample.ViewModel
{
    public class BookViewModel: ViewModelBase
    {
        public ObservableCollection<Book> Books { get; set; }
        public RelayCommand<Book> AddBook { get; set; }

        public BookViewModel()
        {
            Books = new ObservableCollection<Book>(BookManager.GetBooks());
            AddBook = new RelayCommand<Book>((b) => { HandleAddBook(b); });
        }

        private async void HandleAddBook(Book b)
        {
            await new MessageDialog("Added book to cart: " + b.Title + " by " + b.Author, "Book Added!").ShowAsync();
        }
    }
}
