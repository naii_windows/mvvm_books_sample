﻿using mvvm_books_sample.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mvvm_books_sample.Utils
{
    public static class BookManager
    {
        public static List<Book> GetBooks()
        {
            var books = new List<Book>();

            books.Add(new Book
            {
                Title = "The Color Purple",
                Author = "Alice Walker",
                BookCover = "../Assets/BookCovers/color_purple.png"
            });

            books.Add(new Book
            {
                Title = "One Second After",
                Author = "William R. Forstchen",
                BookCover = "../Assets/BookCovers/One_Second_After_cover.png"
            });

            books.Add(new Book
            {
                Title = "The River",
                Author = "Peter Heller",
                BookCover = "../Assets/BookCovers/the_river.png"
            });

            return books;
        }
    }
}
