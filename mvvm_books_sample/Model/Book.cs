﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mvvm_books_sample.Model
{
    public class Book : ObservableObject
    {
        private string title;
        private string author;
        private string bookCover;


        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                Set<string>(() => this.Title, ref title, value);
            }
        }

        public string Author
        {
            get
            {
                return author;
            }
            set
            {
                Set<string>(() => this.Author, ref author, value);
            }
        }

        public string BookCover
        {
            get
            {
                return bookCover;
            }
            set
            {
                Set<string>(() => this.BookCover, ref bookCover, value);
            }
        }
    }
}
